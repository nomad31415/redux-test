
/*
import React,{ Component } from 'react';
import {Card, CardBody, CardImg, CardText, CardTitle} from 'reactstrap';




class DishDetail extends Component{
    constructor(props){
        super(props);
        
    }
    

    //function to render selected dish details
    renderDish(dish){
        if(dish!==null){
            return(
                <Card>
                    <CardImg top src={dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>   
                        <CardText> {dish.description} </CardText>
                    </CardBody>
                </Card>          
            )
        }else{
            return(<div></div>)
        }
    }
    //function to render comments for selected dish
    renderComments(dish){
        if(dish!==null){
            if(dish.comments!==null){
                const dish_comments= dish.comments.map((dishComment)=>{
                    var options = {  year: 'numeric', month: 'long', day: 'numeric' };
                    var date= new Date(dishComment.date).toLocaleDateString("en-US", options)
                        return(
                        <ul className="list-unstyled">     
                            <li key={dishComment.id} className="mb-4">
                                {dishComment.comment}
                            </li>
                            <li className="mb-4">-- {dishComment.author}, {date}</li>    
                        </ul>
                        )
                    })
                    return(
                        <div>
                            <h4>Comments</h4>
                            {dish_comments}
                        </div>
                )
            }else{
            return(<div></div>)
            }
        }else{
            return(<div></div>)
        }
    }


    render(){
        const dish=this.props.SelectedDish;
        
        
        return(
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    {this.renderDish(dish)}
                </div>
                <div className="col-12 col-md-5 m-1">            
                    {this.renderComments(dish)}
                </div>
            </div>
        )    
    }
}

export default DishDetail;
*/



/*

222222222222222222222222222

import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle } from 'reactstrap';

class DishDetail extends Component {
	render() {

		const dish = this.props.SelectedDish

		if(dish == null) return <div></div>

		return (
			<div>
				<div className='row'>
					<div className='col-12 col-md-5 m-1'>
						<Card>
							<CardImg width="100%" src={dish.image} alt={dish.name}/>
							<CardBody>
								<CardTitle>{dish.name}</CardTitle>
								<CardText>{dish.description}</CardText>
							</CardBody>
						</Card>
					</div>
					<div className='col-12 col-md-5 m-1'>
					<h4>Customer Comments</h4>
						{dish.comments.map(comment => {
								const shortDate = comment.date.slice(0,10);
							return (
								<div key={comment.id}>
									<p className="bg-primary text-white">{comment.comment}</p>
									<p>{`${comment.author} ${shortDate}`}</p>
								</div>
							)
						})}
					</div>
				</div>
			</div>
		)
		
	}
}

export default DishDetail


*/

import React from 'react';
//import { Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle } from 'reactstrap';

import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';







    function RenderDish(dish) {
        return (
            <Card >
                <CardImg top src={dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        )
    }






    function RenderComments(dish) {
        if(dish!==null){
            if(dish.comments!==null){
                const dish_comments= dish.comments.map((dishComment)=>{
                    var options = {  year: 'numeric', month: 'long', day: 'numeric' };
                    var date= new Date(dishComment.date).toLocaleDateString("en-US", options)
                        return(
                        <ul className="list-unstyled">     
                            <li key={dishComment.id} className="mb-4">
                                {dishComment.comment}
                            </li>
                            <li className="mb-4">-- {dishComment.author}, {date}</li>    
                        </ul>
                        )
                    })
                    return(
                        <div>
                            <h4>Comments</h4>
                            {dish_comments}
                        </div>
                )
            }else{
            return(<div></div>)
            }
        }else{
            return(<div></div>)
        }
    }








    const DishDetail = (props) => {
        if (props.dish != null) {
            return (
                <div className="container">
                <div className="row">
                    <Breadcrumb>

                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>                
                </div>

                
                <div className="row">
                    <div className="col-12 col-md-5 m-1">
                    <Card>
							<CardImg width="100%" src={props.dish.image} alt={props.dish.name}/>
							<CardBody>
								<CardTitle>{props.dish.name}</CardTitle>
								<CardText>{props.dish.description}</CardText>
							</CardBody>
						</Card>
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <RenderComments comments={props.comments} />
                    </div>
                </div>



            





                </div>
            )
        } else {
            return (<div></div>);
        };
    }



export default DishDetail;