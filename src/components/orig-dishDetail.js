import React, { Component } from 'react';
import { Card,CardImg,CardImgOverlay,CardText,CardBody, CardTitle } from 'reactstrap';



class  Dishdetail extends Component {
  constructor(props) {
    super(props);
}

renderDish(dish) {
    if (dish != null)
        return(
            <td><Card>
                <CardImg top src={dish.image} alt={dish.name} />
                <CardBody>
                  <CardTitle>{dish.name}</CardTitle>
                  <CardText>{dish.description}</CardText>  
                </CardBody>
            </Card></td>
        );
    else
        return(
            <div></div>
        );
}

renderComments(dish) {
    let temp;
    if (dish != null)
    {
    temp =  dish.comments.map((comment) => {
        return(
            <Card>
                <CardBody>        
                  <CardText>{comment.comment}</CardText>
                  <CardText>--{comment.author}, {(comment.date).substring(0,comment.date.indexOf("T"))}</CardText>
                </CardBody>
            </Card>
        );});
    }
    else
        return(
            <div></div>
        );
        return (<td valign = "top" ><h4 top>Comments</h4>{temp}</td>);
}

render() {
      return (
           [ this.renderDish(this.props.SelectedDish), this.renderComments(this.props.SelectedDish)]
      );
  }
}
export default  Dishdetail;